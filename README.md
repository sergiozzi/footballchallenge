# Football API Interconnection #

Project to apply data consumption from a public API and persistence in a database. 
Open API for query data.

Site API consumed: [Football-data.org](http://www.football-data.org) 

### Technologies ###

* Microsoft .Net Core 3.1 (Language: C#)
* Entity Framework Core
* API Versioning
* Dependency Injection
* Async Calls
* Mapper Library: Automapper
* SQL Server (Code First)
* Unit test with Xunit and Moq
* Documentation: Swagger

### Author ###

* **Sergio Javier Liuzzi** | sergiozzi@gmail.com