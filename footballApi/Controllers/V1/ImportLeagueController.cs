﻿using AutoMapper;
using footballApi.Common;
using footballApi.Data.Contracts;
using footballApi.Models;
using footballApi.Models.ThirdParties;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static footballApi.Common.ExternalService;

namespace footballApi.Controllers
{
    /// <summary>
    /// Api exposer for Imports
    /// </summary>    
    [ApiVersion("1.0")]    
    [Route("/import-league")]
    public class ImportLeagueController : Controller
    {
        private readonly IConfiguration _configuration;         
        private readonly ILogger<ImportLeagueController> _logger;        
        private readonly IMapper _mapper;
        private readonly ICompetitionRepository _competitionRepository;
        private readonly ICompetitionParticipantRepository _competitionParticipantRepository;
        private readonly ITeamRepository _teamRepository;        

        public ImportLeagueController(IConfiguration configuration,
                                      ILogger<ImportLeagueController> logger,
                                      IMapper mapper,
                                      ICompetitionRepository competitionRepository,
                                      ICompetitionParticipantRepository competitionParticipantRepository,
                                      ITeamRepository teamRepository)
        {
            _configuration = configuration;
            _logger = logger;
            _mapper = mapper;
            _competitionRepository = competitionRepository;
            _competitionParticipantRepository = competitionParticipantRepository;
            _teamRepository = teamRepository;
        }

        /// <summary>
        /// Imports data to a Football League.
        /// </summary>
        /// <param name="leagueCode">League Code</param>
        /// <returns>Action result</returns>
        /// <response code="201">Successfully imported</response>
        /// <response code="404">Not found</response>
        /// <response code="409">League already imported</response>
        /// <response code="504">Server error</response>
        [HttpGet("{leagueCode}")]
        public async Task<IActionResult> Get(string leagueCode)
        {
            try
            {
                if (string.IsNullOrEmpty(leagueCode))
                {
                    return BadRequest();
                }

                leagueCode = leagueCode.ToUpper();
                var league = await _competitionRepository.GetByCode(leagueCode);
                if (league != null)
                {
                    return StatusCode(409, "League already imported");
                }

                string url = String.Empty;
                var responseString = string.Empty;
                ExternalService service = new ExternalService();

                _logger.LogInformation($"Init import to {leagueCode}.");

                var config = _configuration.GetSection("ExternalProviderAPI").Get<ExternalProviderAPI>();

                url = string.Format("{0}competitions/{1}/", config.UrlBase, leagueCode);

                var result = await service.ManageCall(method: HttpMethod.Get,
                                                      resourceUri: url,
                                                      token: config.Token,
                                                      queryParams: null);

                if (result.IsSuccessStatusCode)
                {
                    var competitionExternal = JsonConvert.DeserializeObject<Models.ThirdParties.CompetitionResponse>(result.Content.ReadAsStringAsync().Result);
                    var newCompetition = _mapper.Map<Models.Competition>(competitionExternal);
                    
                    _competitionRepository.Create(newCompetition);
                    await _competitionRepository.SaveChanges();

                    #region Main Logic

                    url = string.Format("{0}competitions/{1}/teams", config.UrlBase, leagueCode);

                    var resultTeams = await service.ManageCall(method: HttpMethod.Get,
                                                               resourceUri: url,
                                                               token: config.Token,
                                                               queryParams: null);

                    if (resultTeams.IsSuccessStatusCode)
                    {                        
                        var teamsExternal = JsonConvert.DeserializeObject<Models.ThirdParties.TeamResponse>(resultTeams.Content.ReadAsStringAsync().Result);

                        if (teamsExternal.count != 0)
                        {
                            List<Models.Team> listTeam = (from item in teamsExternal.teams
                                                          select _mapper.Map<Models.Team>(item)).ToList();

                            foreach (var item in listTeam)
                            {
                                var existingTeam = await _teamRepository.GetByExternalId(item.ExternalId);
                                if (existingTeam != null)
                                {
                                    #region Existing Team

                                    //Verify any relation Competition-Team
                                    var existingRelation = await _competitionParticipantRepository.GetByIdentifiers(newCompetition.Id, existingTeam.Id);
                                    
                                    if (existingTeam == null)
                                    {
                                        //New relation Competition-Team
                                        _competitionParticipantRepository.Create(
                                            new CompetitionParticipant
                                            {
                                                CompetitionId = newCompetition.Id,
                                                TeamId = existingTeam.Id
                                            });

                                        await _competitionParticipantRepository.SaveChanges();
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region New Team and Players

                                    url = string.Format("{0}teams/{1}", config.UrlBase, item.ExternalId);

                                    var resultTeamDetails = await service.ManageCall(method: HttpMethod.Get,
                                                                                      resourceUri: url,
                                                                                      token: config.Token,
                                                                                      queryParams: null);

                                    if (resultTeamDetails.IsSuccessStatusCode)
                                    {
                                        var teamDetailsExternal = JsonConvert.DeserializeObject<TeamDetailsResponse>(resultTeamDetails.Content.ReadAsStringAsync().Result);

                                        if (teamDetailsExternal.squad.Count() != 0)
                                        {
                                            List<Player> listPlayer = (from ply in teamDetailsExternal.squad
                                                                       select _mapper.Map<Player>(ply)).ToList();

                                            item.Players = listPlayer;
                                        }
                                    }

                                    _teamRepository.Create(item);
                                    await _teamRepository.SaveChanges();

                                    //New relation Competition-Team
                                    _competitionParticipantRepository.Create(
                                        new CompetitionParticipant { 
                                            CompetitionId = newCompetition.Id,
                                            TeamId = item.Id
                                    });

                                    await _competitionParticipantRepository.SaveChanges();

                                    #endregion                                   
                                }
                            }                            
                        }
                    }

                    #endregion                   

                    _logger.LogInformation($"Successfully imported. League: { leagueCode}");
                    return StatusCode(201, "Successfully imported");
                }

                return StatusCode(404, "Not found");
            }
            catch (Exception ex)
            {
                _logger.LogError("Critical Error. Details: " + ex.Message);
                Console.WriteLine("Critical Error. Details: " + ex.Message);
                return StatusCode(504, "Server Error");
            }
        }       
    }
}