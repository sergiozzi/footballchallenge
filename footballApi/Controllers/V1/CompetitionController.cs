﻿using AutoMapper;
using footballApi.Data.Contracts;
using footballApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace footballApi.Controllers
{
    /// <summary>
    /// Api exposer for Competitions
    /// </summary>    
    [ApiVersion("1.0")]    
    [Route("/competition")]
    public class CompetitionController : Controller
    {        
        private readonly ILogger<CompetitionController> _logger;        
        private readonly IMapper _mapper;
        private readonly ICompetitionRepository _competitionRepository;        

        public CompetitionController(ILogger<CompetitionController> logger,
                                     IMapper mapper,
                                     ICompetitionRepository competitionRepository)
        {            
            _logger = logger;
            _mapper = mapper;
            _competitionRepository = competitionRepository;            
        }

        /// <summary>
        /// Get a list of Competitions
        /// </summary>
        /// <response code="201">Data found</response>
        /// <response code="404">Not found</response>
        /// <returns></returns>        
        [HttpGet]        
        public async Task<IActionResult> Get()
        {
            try
            {
                var list = await _competitionRepository.GetAll();
                if (list.Count() != 0)
                {
                    _logger.LogInformation(DateTime.UtcNow + " - Calling Competition Get");

                    var response = (from competition in list
                                    select _mapper.Map<CompetitionResponse>(competition)).ToList();

                    return StatusCode(200, response);
                }

                return StatusCode(404, "Not found");
            }
            catch (Exception ex)
            {
                _logger.LogError("Critical Error. Details: " + ex.Message);
                Console.WriteLine("Critical Error. Details: " + ex.Message);
                return StatusCode(504, "Server Error");
            }
        }       
    }
}