﻿using AutoMapper;
using footballApi.Data.Contracts;
using footballApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace footballApi.Controllers
{
    /// <summary>
    /// Api exposer for Teams
    /// </summary>    
    [ApiVersion("1.0")]    
    [Route("/team")]
    public class TeamController : Controller
    {        
        private readonly ILogger<TeamController> _logger;        
        private readonly IMapper _mapper;
        private readonly ITeamRepository _teamRepository;

        public TeamController(ILogger<TeamController> logger,
                              IMapper mapper,
                              ITeamRepository teamRepository)
        {            
            _logger = logger;
            _mapper = mapper;
            _teamRepository = teamRepository;            
        }

        /// <summary>
        /// Get a list of Teams
        /// </summary>
        /// <response code="201">Data found</response>
        /// <response code="404">Not found</response>
        /// <returns></returns>        
        [HttpGet]        
        public async Task<IActionResult> Get()
        {
            try
            {
                var list = await _teamRepository.GetAll();
                if (list.Count() != 0)
                {
                    _logger.LogInformation(DateTime.UtcNow + " - Calling Team Get");

                    var response = (from team in list
                                    select _mapper.Map<TeamResponse>(team)).ToList();

                    return StatusCode(200, response);
                }

                return StatusCode(404, "Not found");
            }
            catch (Exception ex)
            {
                _logger.LogError("Critical Error. Details: " + ex.Message);
                Console.WriteLine("Critical Error. Details: " + ex.Message);
                return StatusCode(504, "Server Error");
            }
        }

        /// <summary>
        /// Returns a list of Team by TLA
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="tla">TLA the Team</param>
        /// <returns>List of Teams</returns>
        /// <response code="200">Returns a list of Teams</response>
        /// <response code="400">Not found</response>        
        [HttpGet("{tla}")]
        public async Task<IActionResult> GetByTla(string tla)
        {
            try
            {
                var list = await _teamRepository.GetByTLA(tla);
                if (list.Count() != 0)
                {
                    _logger.LogInformation(DateTime.UtcNow + " - Calling Team GetByTLA");

                    var response = (from team in list
                                    select _mapper.Map<TeamResponse>(team)).ToList();

                    return StatusCode(200, response);
                }

                return StatusCode(404, "Not found");
            }
            catch (Exception ex)
            {
                _logger.LogError("Critical Error. Details: " + ex.Message);
                Console.WriteLine("Critical Error. Details: " + ex.Message);
                return StatusCode(504, "Server Error");
            }
        }
    }   
}