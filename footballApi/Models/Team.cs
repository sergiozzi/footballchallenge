﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace footballApi.Models
{
    /// <summary>
    /// Class to represent a Team
    /// </summary>
    [Table("Team")]
    public class Team
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string ExternalId { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(3)]
        public string Tla { get; set; }

        [MaxLength(20)]
        public string ShortName { get; set; }

        [MaxLength(50)]
        public string AreaName { get; set; }

        [MaxLength(320)]
        public string Email { get; set; }

        public List<Player> Players { get; set; }
    }
}