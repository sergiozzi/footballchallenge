﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace footballApi.Models
{
    /// <summary>
    /// Class to represent a relation Competition-Team
    /// </summary>
    [Table("CompetitionParticipant")]
    public class CompetitionParticipant
    {
        public int CompetitionId { get; set; }
        public int TeamId { get; set; }

        //Relations
        public Competition Competition { get; set; }

        public Team Team { get; set; }
    }
}