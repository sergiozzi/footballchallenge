﻿using System;

namespace footballApi.Models.ThirdParties
{
    public class CompetitionResponse
    {
        public int id { get; set; }
        public Area area { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string emblemUrl { get; set; }
        public string plan { get; set; }
        public Currentseason currentSeason { get; set; }
        public Season[] seasons { get; set; }
        public DateTime lastUpdated { get; set; }
    }

    public class Area
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Currentseason
    {
        public int id { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public object currentMatchday { get; set; }
        public object winner { get; set; }
    }

    public class Season
    {
        public int id { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int? currentMatchday { get; set; }
        public Winner winner { get; set; }
    }

    public class Winner
    {
        public int id { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string tla { get; set; }
        public string crestUrl { get; set; }
    }
}