﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace footballApi.Models
{
    /// <summary>
    /// Class to represent a Player
    /// </summary>
    [Table("Player")]
    public class Player
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string ExternalId { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }                
        
        public DateTime? DateOfBirth { get; set; }

        [MaxLength(50)]
        public string CountryOfBirth { get; set; }

        [MaxLength(50)]
        public string Nacionality { get; set; }

        [MaxLength(30)]
        public string Position { get; set; }
    }
}