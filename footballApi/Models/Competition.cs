﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace footballApi.Models
{
    /// <summary>
    /// Class to represent a Competition
    /// </summary>
    [Table("Competition")]
    public class Competition
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string ExternalId { get; set; }

        [MaxLength(20)]
        public string Code { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string AreaName { get; set; }

        //public List<Team> Teams { get; set; }
    }
}