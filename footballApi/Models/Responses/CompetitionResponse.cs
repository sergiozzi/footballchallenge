﻿namespace footballApi.Models
{
    public class CompetitionResponse
    {
        public string Code { get; set; }
        public string Denomination { get; set; }
        public string Area { get; set; }
    }
}