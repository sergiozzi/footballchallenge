﻿using System.Collections.Generic;

namespace footballApi.Models
{
    public class TeamResponse
    {
        public string NickName { get; set; }
        public string Denomination { get; set; }
        public string Country { get; set; }
        public string TLA { get; set; }
        public string ContactEmail { get; set; }

        public List<Player> Players { get; set; }
    }
}