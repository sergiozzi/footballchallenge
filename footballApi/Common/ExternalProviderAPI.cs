﻿namespace footballApi.Common
{
    public class ExternalProviderAPI
    {        
        public string UrlBase { get; set; }

        public string Token { get; set; }

        public string Email { get; set; }
    }
}