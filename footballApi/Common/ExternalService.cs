﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace footballApi.Common
{
    public class ExternalService
    {
        /// <summary>
        /// Manages HTTP call to a web service
        /// </summary>
        /// <param name="method">The HTTP method for the call.</param>
        /// <param name="resourceUri">The URI for the call.</param>
        /// <param name="token">The Token for the call.</param>
        /// <param name="queryParams">>The query string parameters for the call (if any).</param>        
        /// <returns>The corresponding HTTP response.</returns>
        public async Task<HttpResponseMessage> ManageCall(HttpMethod method,
                                                          string resourceUri,
                                                          string token,
                                                          Dictionary<string, string> queryParams)
        {
            using var httpClient = new HttpClient();

            try
            {
                var completeUrl = AssembleUrl(resourceUri, queryParams);

                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                var response = await CallAsync(method, httpClient, completeUrl);
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception("Critical Error. Details: " + ex.Message);
            }
        }

        /// <summary>
        /// Makes an async all to a http service.
        /// </summary>
        /// <param name="httpMethod"></param>
        /// <param name="httpClient"></param>
        /// <param name="completeUrl"></param>        
        /// <returns></returns>
        private async Task<HttpResponseMessage> CallAsync(HttpMethod httpMethod,
                                                          HttpClient httpClient,
                                                          string completeUrl)
        {
            switch (httpMethod)
            {
                case HttpMethod.Get:
                    Stopwatch diagnosticGet = Stopwatch.StartNew();
                    var resultGet = await httpClient.GetAsync(completeUrl);
                    diagnosticGet.Stop();
                    
                    Console.WriteLine($"[Time Diagnosis] DateTime(UTC): {DateTime.UtcNow} | Method: Get | Url: {completeUrl} | " +
                                       $"Awaiting response (ms): {diagnosticGet.ElapsedMilliseconds}");
                    return resultGet;

                case HttpMethod.Post:
                    throw new NotImplementedException();

                case HttpMethod.Put:
                    throw new NotImplementedException();

                case HttpMethod.Delete:
                    throw new NotImplementedException();
            }
            return null;
        }

        /// <summary>
        /// Composes an URL with possible query string parameters.
        /// </summary>
        /// <param name="resourceUri">The resource URI.</param>
        /// <param name="queryParams">The list of items with param/value information for querystring parameters.</param>
        /// <returns>String result</returns>
        private string AssembleUrl(string resourceUri, Dictionary<string, string> queryParams)
        {
            var result = new StringBuilder(resourceUri);

            if (queryParams != null && queryParams.Any())
            {
                var index = 1;
                var separator = "?";
                foreach (var queryParam in queryParams)
                {
                    if (index > 1)
                    {
                        separator = "&";
                    }

                    result.Append($"{separator}{HttpUtility.UrlEncode(queryParam.Key)}={queryParam.Value}");
                    index++;
                }
            }
            return result.ToString();
        }

        public enum HttpMethod
        {
            Get = 1,
            Post = 2,
            Put = 3,
            Delete = 4
        }
    }
}