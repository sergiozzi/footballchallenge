﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace footballApi.Migrations
{
    public partial class FixTeamEmailDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Team",
                maxLength: 320,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldMaxLength: 320);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Email",
                table: "Team",
                type: "int",
                maxLength: 320,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 320,
                oldNullable: true);
        }
    }
}
