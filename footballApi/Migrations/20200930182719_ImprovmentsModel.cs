﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace footballApi.Migrations
{
    public partial class ImprovmentsModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Team_Competition_CompetitionId",
                table: "Team");

            migrationBuilder.DropIndex(
                name: "IX_Team_CompetitionId",
                table: "Team");

            migrationBuilder.DropColumn(
                name: "CompetitionId",
                table: "Team");

            migrationBuilder.CreateTable(
                name: "CompetitionParticipant",
                columns: table => new
                {
                    CompetitionId = table.Column<int>(nullable: false),
                    TeamId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitionParticipant", x => new { x.CompetitionId, x.TeamId });
                    table.ForeignKey(
                        name: "FK_CompetitionParticipant_Competition_CompetitionId",
                        column: x => x.CompetitionId,
                        principalTable: "Competition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompetitionParticipant_Team_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Team",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompetitionParticipant_TeamId",
                table: "CompetitionParticipant",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetitionParticipant");

            migrationBuilder.AddColumn<int>(
                name: "CompetitionId",
                table: "Team",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Team_CompetitionId",
                table: "Team",
                column: "CompetitionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Team_Competition_CompetitionId",
                table: "Team",
                column: "CompetitionId",
                principalTable: "Competition",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
