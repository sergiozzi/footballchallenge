﻿using footballApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace footballApi.Data.Contracts
{
    public interface IPlayerRepository
    {
        void Create(Player pyr);
        Task<bool> SaveChanges();
        Task<Player> GetById(int id);
        Task<IEnumerable<Player>> GetAll();        
    }
}