﻿using footballApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace footballApi.Data.Contracts
{
    public interface ITeamRepository
    {
        void Create(Team pyr);
        Task<bool> SaveChanges();
        Task<Team> GetById(int id);
        Task<Team> GetByExternalId(string id);
        Task<IEnumerable<Team>> GetByTLA(string tla);
        Task<IEnumerable<Team>> GetAll();        
    }
}