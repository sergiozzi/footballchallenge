﻿using footballApi.Models;
using System.Threading.Tasks;

namespace footballApi.Data.Contracts
{
    public interface ICompetitionParticipantRepository
    {
        void Create(CompetitionParticipant cp);
        Task<bool> SaveChanges();      
        Task<CompetitionParticipant> GetByIdentifiers(int competitionId, int teamId);        
    }
}