﻿using footballApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace footballApi.Data.Contracts
{
    public interface ICompetitionRepository
    {
        void Create(Competition cmp);
        Task<bool> SaveChanges();
        Task<Competition> GetById(int id);
        Task<Competition> GetByCode(string code);
        Task<IEnumerable<Competition>> GetAll();        
    }
}