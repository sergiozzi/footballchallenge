﻿using footballApi.Data.Contracts;
using footballApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace footballApi.Data.Repositories
{
    public class CompetitionParticipantRepository : ICompetitionParticipantRepository
    {
        private readonly FootballContext _context;

        public CompetitionParticipantRepository(FootballContext context)
        {
            _context = context;
        }

        public void Create(CompetitionParticipant cp)
        {
            if (cp == null)
            {
                throw new ArgumentNullException(nameof(cp));
            }

            _context.CompetitionsParticipants.Add(cp);
        }        

        public async Task<CompetitionParticipant> GetByIdentifiers(int competitionId, int teamId)
        {
            return await _context.CompetitionsParticipants
                                .Where(x => x.CompetitionId == competitionId && x.TeamId == teamId)
                                .FirstOrDefaultAsync();
        }

        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }        
    }
}