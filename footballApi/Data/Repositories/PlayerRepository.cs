﻿using footballApi.Data.Contracts;
using footballApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballApi.Data.Repositories
{
    /// <summary>
    /// Player Repository
    /// </summary>
    public class PlayerRepository : IPlayerRepository
    {
        private readonly FootballContext _context;

        /// <summary>
        /// Constructor for Player Repository
        /// </summary>
        /// <param name="context">Context</param>
        public PlayerRepository(FootballContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="pyr">Player</param>
        public void Create(Player pyr)
        {
            if (pyr == null)
            {
                throw new ArgumentNullException(nameof(pyr));
            }

            _context.Players.Add(pyr);
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns>Players</returns>
        public async Task<IEnumerable<Player>> GetAll()
        {
            var list = await _context.Players
                                    .OrderBy(x => x.Name)
                                    .ToListAsync();
            return list;
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Player</returns>
        public async Task<Player> GetById(int id)
        {
            return await _context.Players
                                .Where(x => x.Id == id)
                                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Save Changes
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }        
    }
}