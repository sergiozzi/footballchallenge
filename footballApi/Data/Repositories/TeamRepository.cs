﻿using footballApi.Data.Contracts;
using footballApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballApi.Data.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private readonly FootballContext _context;

        public TeamRepository(FootballContext context)
        {
            _context = context;
        }

        public void Create(Team tm)
        {
            if (tm == null)
            {
                throw new ArgumentNullException(nameof(tm));
            }

            _context.Teams.Add(tm);
        }

        public async Task<IEnumerable<Team>> GetAll()
        {
            var list = await _context.Teams
                                    .OrderBy(x => x.Name)
                                    .ToListAsync();
            return list;
        }

        public async Task<IEnumerable<Team>> GetByTLA(string tla)
        {
            var list = await _context.Teams
                                    .Include(pl => pl.Players)
                                    .Where(x => x.Tla == tla)
                                    .OrderBy(x => x.Name)
                                    .ToListAsync();
            return list;
        }

        public async Task<Team> GetById(int id)
        {
            return await _context.Teams
                                .Where(x => x.Id == id)
                                .FirstOrDefaultAsync();
        }

        public async Task<Team> GetByExternalId(string id)
        {
            return await _context.Teams                                
                                .Where(x => x.ExternalId == id)
                                .FirstOrDefaultAsync();
        }        

        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }        
    }
}