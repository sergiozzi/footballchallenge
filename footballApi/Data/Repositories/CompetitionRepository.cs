﻿using footballApi.Data.Contracts;
using footballApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballApi.Data.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class CompetitionRepository : ICompetitionRepository
    {
        private readonly FootballContext _context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public CompetitionRepository(FootballContext context)
        {
            _context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmp"></param>
        public void Create(Competition cmp)
        {
            if (cmp == null)
            {
                throw new ArgumentNullException(nameof(cmp));
            }

            _context.Competitions.Add(cmp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Competition>> GetAll()
        {
            var list = await _context.Competitions
                                    .OrderBy(x => x.Name)
                                    .ToListAsync();
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Competition> GetById(int id)
        {
            return await _context.Competitions
                                .Where(x => x.Id == id)
                                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<Competition> GetByCode(string code)
        {
            return await _context.Competitions
                                .Where(x => x.Code == code)
                                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }        
    }
}