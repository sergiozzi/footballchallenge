﻿using footballApi.Models;
using Microsoft.EntityFrameworkCore;

namespace footballApi.Data
{
    public class FootballContext : DbContext
    {
        #region Constructor

        /// <summary>
        /// Creates a new football db context with a generilized options
        /// </summary>
        /// <param name="options"></param>
        public FootballContext(DbContextOptions<FootballContext> opts) : base(opts)
        {
        }

        #endregion

        #region DbSets
                
        public DbSet<Competition> Competitions { get; set; }
                
        public DbSet<Team> Teams { get; set; }
                
        public DbSet<Player> Players { get; set; }

        public DbSet<CompetitionParticipant> CompetitionsParticipants { get; set; }

        #endregion

        #region Overrides

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CompetitionParticipant>()
                .HasKey(x => new { x.CompetitionId, x.TeamId });
        }        

        #endregion
    }
}