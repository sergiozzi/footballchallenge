﻿using AutoMapper;
using System;

namespace footballApi.Profiles
{
    public class PlayerProfile : Profile
    {
        public PlayerProfile()
        {
            CreateMap<Models.ThirdParties.Squad, Models.Player>()
                 .ForMember(d => d.Id, o => o.Ignore())
                 .ForMember(d => d.ExternalId, o => o.MapFrom(s => s.id))
                 .ForMember(d => d.Name, o => o.MapFrom(s => s.name))
                 .ForMember(d => d.DateOfBirth, o => o.MapFrom(s => s.dateOfBirth == null ? null : s.dateOfBirth))
                 .ForMember(d => d.CountryOfBirth, o => o.MapFrom(s => s.countryOfBirth))
                 .ForMember(d => d.Nacionality, o => o.MapFrom(s => s.nationality))
                 .ForMember(d => d.Position, o => o.MapFrom(s => s.position));
        }
    }
}