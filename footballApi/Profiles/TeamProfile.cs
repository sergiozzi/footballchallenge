﻿using AutoMapper;

namespace footballApi.Profiles
{
    /// <summary>
    /// Automapper profile to Team
    /// </summary>
    public class TeamProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public TeamProfile()
        {
            CreateMap<Models.ThirdParties.Team, Models.Team>()
                 .ForMember(d => d.Id, o => o.Ignore())
                 .ForMember(d => d.ExternalId, o => o.MapFrom(s => s.id))
                 .ForMember(d => d.Name, o => o.MapFrom(s => s.name))
                 .ForMember(d => d.Tla, o => o.MapFrom(s => s.tla))
                 .ForMember(d => d.ShortName, o => o.MapFrom(s => s.shortName))
                 .ForMember(d => d.Email, o => o.MapFrom(s => s.email))
                 .ForPath(d => d.AreaName, o => o.MapFrom(s => s.area.name));

            CreateMap<Models.Team, Models.TeamResponse>()
                 .ForMember(d => d.NickName, o => o.MapFrom(s => s.ShortName))
                 .ForMember(d => d.Denomination, o => o.MapFrom(s => s.Name))
                 .ForMember(d => d.Country, o => o.MapFrom(s => s.AreaName))
                 .ForMember(d => d.ContactEmail, o => o.MapFrom(s => s.Email));
        }
    }
}