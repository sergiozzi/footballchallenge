﻿using AutoMapper;

namespace footballApi.Profiles
{
    public class CompetitionProfile : Profile
    {
        public CompetitionProfile()
        {
            CreateMap<Models.ThirdParties.CompetitionResponse, Models.Competition>()
                 .ForMember(d => d.Id, o => o.Ignore())
                 .ForMember(d => d.ExternalId, o => o.MapFrom(s => s.id))
                 .ForMember(d => d.Name, o => o.MapFrom(s => s.name))
                 .ForMember(d => d.Code, o => o.MapFrom(s => s.code))
                 .ForPath(d => d.AreaName, o => o.MapFrom(s => s.area.name));

            CreateMap<Models.Competition, Models.CompetitionResponse>()
                 .ForMember(d => d.Code, o => o.MapFrom(s => s.Code))
                 .ForMember(d => d.Denomination, o => o.MapFrom(s => s.Name))
                 .ForMember(d => d.Area, o => o.MapFrom(s => s.AreaName));
        }
    }
}