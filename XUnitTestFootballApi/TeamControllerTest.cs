using footballApi.Controllers;
using footballApi.Data.Contracts;
using footballApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Xunit;

namespace XUnitTestFootballApi
{
    public class TeamControllerTests
    {
        private readonly Mock<HttpContext> _context;
        private readonly Mock<ILogger<TeamController>> _logger;
        private readonly TeamController _classUnderTest;
        private readonly Mock<ITeamRepository> _teamRepository;

        public TeamControllerTests()
        {
            _logger = new Mock<ILogger<TeamController>>();
            _context = new Mock<HttpContext>();
            _teamRepository = new Mock<ITeamRepository>();

            _classUnderTest = new TeamController(_logger.Object,
                                                new TestMapper().CreateMapper(),
                                                _teamRepository.Object)
            {
                ControllerContext = new ControllerContext()
                {
                    HttpContext = _context.Object
                }
            };
        }

        [Fact]
        public async Task Get_ShouldGetListOfTeams()
        {
            List<Player> mockPlayers = new List<Player>
            {
                new Player { Id = 1, Name = "Player 1", Nacionality = "Argentina" },
                new Player { Id = 2, Name = "Player 2", Nacionality = "Argentina" },
                new Player { Id = 3, Name = "Player 3", Nacionality = "Argentina" }
            };

            List<Team> teams = new List<Team>
            {
                new Team {
                    Id = 1,
                    Name = "Team Test",
                    AreaName = "Argentina",
                    ShortName = "Test",
                    ExternalId = "123",
                    Tla = "TLA",
                    Players = mockPlayers
                },
                new Team {
                    Id = 2,
                    Name = "Team ASD",
                    AreaName = "Argentina",
                    ShortName = "Test ASD",
                    ExternalId = "123456",
                    Tla = "ASD",
                    Players = null
                },
            };

            _teamRepository.Setup(a => a.GetAll())
                           .ReturnsAsync(teams)
                           .Verifiable();

            var getResult = await _classUnderTest.Get();

            var getResponse = Assert.IsType<ObjectResult>(getResult);

            var getObj = Assert.IsType<List<TeamResponse>>(getResponse.Value);

            Assert.NotNull(getResult);
            Assert.Equal(2, getObj.Count());            

            _teamRepository.Verify();
        }

        [Fact]
        public async Task GetByTLA_ShouldGetATeam()
        {
            string tla = "TLA";

            List<Player> mockPlayers = new List<Player>
            {
                new Player { Id = 1, Name = "Player 1", Nacionality = "Argentina" },
                new Player { Id = 2, Name = "Player 2", Nacionality = "Argentina" },
                new Player { Id = 3, Name = "Player 3", Nacionality = "Argentina" }
            };

            List<Team> teams = new List<Team>
            {
                new Team { 
                    Id = 1,
                    Name = "Team Test",
                    AreaName = "Argentina",
                    ShortName = "Test",
                    ExternalId = "123",
                    Tla = "TLA",
                    Players = mockPlayers
                }
            };

            _teamRepository.Setup(a => a.GetByTLA(tla))
                           .ReturnsAsync(teams)
                           .Verifiable();

            var getResult = await _classUnderTest.GetByTla(tla);

            var getResponse = Assert.IsType<ObjectResult>(getResult);

            var getObj = Assert.IsType<List<TeamResponse>>(getResponse.Value);

            Assert.NotNull(getResult);
            Assert.NotNull(getObj.FirstOrDefault().NickName);
            Assert.NotNull(getObj.FirstOrDefault().Players);

            _teamRepository.Verify();
        }        
    }
}