﻿using AutoMapper;
using footballApi.Profiles;

namespace XUnitTestFootballApi
{
    public class TestMapper : MapperConfiguration
    {
        public TestMapper()
             : base(cfg =>
             {
                 cfg.AddProfile<CompetitionProfile>();
                 cfg.AddProfile<PlayerProfile>();
                 cfg.AddProfile<TeamProfile>();                 
             })
        { }
    }
}